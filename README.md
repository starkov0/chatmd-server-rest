#README (please)

##Set-up
You need MySQL or MariaDB (it's the same) installed with a user 'chat' (pw: 
chat). It needs to have a table 'chat' with all rights granted to it.

Then load the tables: `./scripts/loadTables`



##How to launch this code
simply run
./scripts/launch_server.sh

Be sure you have updated tables. For that, run the following script:
`./scripts/loadTables.sh`

##Added a new model ?
If you've added a new model, you have to
1. Create the model in your database (aka SQL)
2. Accordingly modify the chat.sql database scheme or run:
`./scripts/dumpTables.sh` if you've done so using phpmyadmin


