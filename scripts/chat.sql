-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: chat
-- ------------------------------------------------------
-- Server version	5.5.43-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `conversation_msgs`
--

DROP TABLE IF EXISTS `conversation_msgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_msgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_msgs`
--

LOCK TABLES `conversation_msgs` WRITE;
/*!40000 ALTER TABLE `conversation_msgs` DISABLE KEYS */;
INSERT INTO `conversation_msgs` VALUES (3,2,1,'aouaou oau ae','0000-00-00 00:00:00'),(4,1,3,'Bonjour à tous, vous allez bien ?','2015-06-09 11:28:21'),(5,1,3,'Répondez-moi, bande de cons !','2015-06-09 11:28:45'),(6,1,3,'Eh oh ! Salauds é !!!','2015-06-09 11:29:34'),(7,1,3,'Eh oh ! Salauds !!!','2015-06-09 18:31:58'),(8,2,3,'Je suis une réponse !','2015-06-09 18:32:12'),(9,1,5,'Salut les gars, vous allez bien ?','0000-00-00 00:00:00'),(10,2,5,'HEy !\r\nOuais, j\'ai vu un patient cancéreux, j\'lui ait dit qui va plus couter cher à la sécu','0000-00-00 00:00:00'),(11,3,5,'Hahah, salaud !','0000-00-00 00:00:00'),(12,4,5,'Notre créateur est schizophrène','0000-00-00 00:00:00'),(13,1,5,'laul','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `conversation_msgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_users`
--

DROP TABLE IF EXISTS `conversation_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `crypto_key_pwd` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_users`
--

LOCK TABLES `conversation_users` WRITE;
/*!40000 ALTER TABLE `conversation_users` DISABLE KEYS */;
INSERT INTO `conversation_users` VALUES (4,2,1,'','2015-06-01 00:00:00'),(5,3,1,'','2015-06-04 00:00:00'),(6,3,2,'','2015-06-15 00:00:00'),(7,1,3,'','0000-00-00 00:00:00'),(8,1,5,'','0000-00-00 00:00:00'),(9,2,5,'','0000-00-00 00:00:00'),(10,3,5,'','0000-00-00 00:00:00'),(11,4,5,'','0000-00-00 00:00:00'),(12,2,3,'','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `conversation_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `crypto_key` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversations`
--

LOCK TABLES `conversations` WRITE;
/*!40000 ALTER TABLE `conversations` DISABLE KEYS */;
INSERT INTO `conversations` VALUES (1,'Convo 1','','0000-00-00 00:00:00'),(2,'Conversation intéressante 2','','0000-00-00 00:00:00'),(3,'ma conversation REST','generated crypto key','2015-06-09 11:21:51'),(5,'Tout le monde dedans !','toto','2015-06-11 18:42:28');
/*!40000 ALTER TABLE `conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` varchar(300) NOT NULL,
  `begin` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiences`
--

LOCK TABLES `experiences` WRITE;
/*!40000 ALTER TABLE `experiences` DISABLE KEYS */;
INSERT INTO `experiences` VALUES (1,1,'My experience 1','Description 1 - user 1',1432308677,1431308677,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,'My experience 2','Description 2 - user 1',2015,2015,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,2,'aouoau','aouoauoau',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,3,'aouaouao','aouaouoauoauaou',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friendships`
--

DROP TABLE IF EXISTS `friendships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friendships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_initiator_id` int(11) NOT NULL,
  `user_asked_id` int(11) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `accepted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friendships`
--

LOCK TABLES `friendships` WRITE;
/*!40000 ALTER TABLE `friendships` DISABLE KEYS */;
INSERT INTO `friendships` VALUES (3,5,2,'vive le monde',0,'2015-05-22 23:25:50','2015-05-22 23:25:50'),(4,1,5,'vive le monde',0,'2015-05-22 23:28:15','2015-05-22 23:28:15'),(17,2,1,'vive le monde',1,'2015-05-23 00:22:13','2015-05-23 00:24:37');
/*!40000 ALTER TABLE `friendships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `lattitude` float NOT NULL,
  `longitude` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Test Geneve',2,222,1,'0000-00-00 00:00:00','2015-06-04 17:03:31'),(2,'Je suis a Boston',-42,42,2,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (3,'my title','my description',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'my title','my description',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'my title','my description',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'my title','my description',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'my title','my description',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'my title','my ououoauoaudescription',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'toto','tutu',1,'2015-05-31 21:56:36','2015-05-31 21:56:36');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `title` varchar(55) NOT NULL,
  `abstract` varchar(300) NOT NULL,
  `hobbies` varchar(300) NOT NULL,
  `profile_pic` varchar(50) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `password_hash` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'toto@tutu.com','Mr big doctor','XXXXXXist','Hey, I\'m good at what I do','Eating fish','PIERRE.jpg',0,'','0000-00-00 00:00:00','2015-06-09 21:31:26'),(2,'yopyop@gmail.com','Mr Mcdonalds','My super title','osuhao usoah oasutnha usoatnhu oasunh ','Eating yo mama','FRANK.jpg',0,'','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'steve@jobs.com','Steve jobs','Mr. informatic doctor','I love apple','Computing','Jobs.jpg',1,'','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'stewe@wozniak.com','Steve Wozniak','Geek nerd','I love nerding','And climbing','Wozniak.jpg',1,'','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-22 23:24:40
