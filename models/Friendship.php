<?php
/*
** Friendship.php - user friendships
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: See above
*/

class Friendship extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  user_initiator_id (FORGEIN)
	//  user_asked_id (FORGEIN)
	//  comment (VARCHAR)
	//  accepted (BOOLEAN)

	static $belongs_to = array(
		array('user_initiator', 'class_name' => 'User'),
		array('user_asked', 'class_name' => 'User')
	);

	static $validates_length_of = array(
		array('comment', 'maximum' => 300)
	);

	static $validates_uniqueness_of = array(
		array(array('user_initiator_id', 'user_asked_id'),
		      'message' => 'Tuples must be unique')
	);

	public function after_validation() {
		//invalidate (x, x): self-friend
		if($this->user_asked_id == $this->user_initiator_id) {
			$this->errors->add('user_initiator_id', 'FU');
		}
	}

	public function after_validation_on_create() {
		//$validates_uniqueness_of can't validate ((x, y) and (y, x)): they
		//are the same. So we do-it here:

		//we get the oposites
		if(FriendshipController::friendship_exists(
		   $this->user_initiator_id, $this->user_asked_id)) {
			$this->errors->add('user_initiator_id',
			                   'opposite friendship already in request');
		}
	}


	public static function find_all_by_user_asked_and_user_initiator(
	                        $asked_id, $initiator_id) {
		return Friendship::all(
		         array('conditions' =>
		                array('user_asked_id = ? AND user_initiator_id = ?',
		                      $asked_id, $initiator_id)
		              )
		);
	}
}

/* vim: set ts=4 sw=4 noet: */

