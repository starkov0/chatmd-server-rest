<?php
/*
** Skill.php - Not Skil tools
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Skill of a User
*/

class Skill extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  title (VARCHAR)
	//  description (VARCHAR)
	//  user_id (FORGEIN)

	static $belongs_to = array(
		array('user')
	);

	static $validates_length_of = array(
		array('title', 'maximum' => 55),
		array('description', 'maximum' => 300)
	);
}

Authority::allow('manage', 'Skill', function ($auth_user, $a_skill) {
	return $auth_user->equalsTo($a_skill->user);
});

Authority::allow('create', 'Skill', function ($auth_user, $obj) {
	return $auth_user->canUseWebsite();
});


/* vim: set ts=4 sw=4 noet: */

