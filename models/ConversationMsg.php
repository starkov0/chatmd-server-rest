<?php
/*
** ConversationMsg.php - Message of a conversation
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: See above
*/

class ConversationMsg extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  user_id (FORGEIN)
	//  conversation_id (FORGEIN)
	//  content (TEXT)
	//  created_at

	static $table_name = 'conversation_msgs';

	static $belongs_to = array(
		array('User'),
		array('Conversation')
	);

	static $validates_size_of = array(
		array('content', 'within' => array(1, 2000))
	);
}

Authority::allow('delete', 'ConversationMsg', function ($auth_user, $a_msg) {
	//I am the message owner
	return $auth_user->equalsTo($a_msg->user);
});

Authority::allow('create', 'ConversationMsg', function ($auth_user, $a_msg) {
	//To post in a conversation, I must belong to the said conversation
	return $a_msg->conversation->userBelongs($auth_user);
});


/* vim: set ts=4 sw=4 noet: */

