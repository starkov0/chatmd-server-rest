<?php
/*
** ConversationUser.php - Links a user to a conversation
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: See above
*/

class ConversationUser extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  user_id (FORGEIN)
	//  conversation_id (FORGEIN)
	//  crypto_key_msg (VARCHAR)
	//  created_at

	static $table_name = 'conversation_users';

	static $belongs_to = array(
		array('conversation'),
		array('user')
	);

	static $validates_uniqueness_of = array(
		array(array('user_id', 'conversation_id'),
		      'message' => 'Can only follow a conversation once, jackass  !')
	);
}

Authority::allow('manage', 'ConversationUser', function ($auth_user, $obj) {
	//Must be member of convo
	$convousers = ConversationUser::find_all_by_conversation_id($obj->conversation_id);
	foreach($convousers as $convouser) {
		if($convouser->user->id == $auth_user->id) {
			return 1;
		}
	}

	return 0;
});

Authority::allow('delete', 'ConversationUser', function ($auth_user, $obj) {
	//The user itself
	if($auth_user->equalsTo($obj->user))
		return 1;

	return 0;
});


/* vim: set ts=4 sw=4 noet: */

