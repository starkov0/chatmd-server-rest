<?php
/*
** Conversation.php - Conversations
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: See above
*/

class Conversation extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  title
	//  crypto_key (key)
	//  created_at

	static $has_many = array(
		array('conversation_msgs'),
		array('conversation_users')
	);

	static $belongs_to = array(
		array('User')
	);

	static $validates_size_of = array(
		array('title', 'within' => array(1, 300))
	);


	/*
	 * Does a user belongs to a conversation (aka. ConversationUser)
	 */
	public function userBelongs(User $u) {
		$conversationusers_of_convo = $this->conversation_users;

		foreach($conversationusers_of_convo as $CU) {
			if($u->equalsTo($CU->user))
				return 1;
		}
		return 0;
	}
}


//The user must be included in the conversation
Authority::allow('read', 'Conversation', function ($auth_user, $a_conversation) {
	return $a_conversation->userBelongs($auth_user);
});

Authority::allow('create', 'Conversation', function ($auth_user, $obj) {
	return $auth_user->canUseWebsite();
});

/* vim: set ts=4 sw=4 noet: */

