<?php
/*
** User.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A user
*/

class User extends ActiveRecord\Model {
	//ATTRIBUTES:
	//  email
	//  name
	//
	//  title
	//  abstract
	//  hobbies
	//
	//  image
	//
	//  active
	//  online ??
	//  verified ??
	//
	//  last_login ??
	//  created_at ??
	//
	//  password_hash
	//  super_password_hash ??
	//
	//  encrypted_user_key
	//  encrypted_super_user_key
	//
	//  encrypted_private_key
	//  public_key
	//
	//  --- FORGEIN OBJECTS ---
	//
	//  location
	//  skils

	static $has_one = array(
		array('location')
	);

	static $has_many = array(
		array('skills'),
		array('experiences'),
		array('conversation_users'), //Which is a participant
		array('conversations'), //Which is the owner of
		array('conversation_msgs') //Which is the owner of
	);

	static $validates_size_of = array(
		array('email', 'within' => array(1, 55)),
		array('name', 'within' => array(1, 55)),
	);

	public function equalsTo(User $u) {
		return $this->id == $u->id;
	}

	public function canUseWebsite() {
		//we can edit this function if we implement sometimes banning users
		return $this->id > 0;
	}

}

/* vim: set ts=4 sw=4 noet: */

