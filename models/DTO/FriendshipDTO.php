<?php
/*
** FriendshipDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A Friendship DTO
*/

class FriendshipDTO extends DTO {

	public $id;
	public $user_initiator_id;
	public $user_asked_id;
	public $comment;
	public $accepted;


	public function __construct(Friendship $friendship) {
		$this->id = $friendship->id;
		$this->user_initiator_id = $friendship->user_initiator_id;
		$this->user_asked_id = $friendship->user_asked_id;
		$this->comment = $friendship->comment;
		$this->accepted = $friendship->accepted;
	}

}

/* vim: set ts=4 sw=4 noet: */

