<?php
/*
** ExperienceDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: An Experience DTO
*/

class ExperienceDTO extends DTO {

	public $id;
	public $title;
	public $description;
//	public $begin;
//	public $end;


	public function __construct(Experience $Experience) {
		$this->id = $Experience->id;
		$this->title = $Experience->title;
		$this->description = $Experience->description;
//		$this->begin = date('Y-m-d', $Experience->begin);
//		$this->begin = date('c', $Experience->begin);
//		$this->end = date('c', $Experience->end);
	}

}

/* vim: set ts=4 sw=4 noet: */

