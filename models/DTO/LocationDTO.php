<?php
/*
** LocationDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A Location DTO
*/

class LocationDTO extends DTO {

	public $id;
	public $name;
	public $lattitude;
	public $longitude;

	public function __construct(Location $location) {
		$this->id = $location->id;
		$this->name = $location->name;
		$this->lattitude = $location->lattitude;
		$this->longitude = $location->longitude;
	}

}

/* vim: set ts=4 sw=4 noet: */

