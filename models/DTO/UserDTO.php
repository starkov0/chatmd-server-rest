<?php
/*
** UserDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A user DTO
*/

class UserDTO extends DTO {

	public $id;
	public $name;
	public $title;
	public $abstract;
	public $hobbies;
	public $profile_pic;

	public function __construct(User $user) {
		$this->id = $user->id;

		$this->name = $user->name;
		$this->title = $user->title;
		$this->abstract = $user->abstract;
		$this->hobbies = $user->hobbies;
		$this->profile_pic = $user->profile_pic;
	}

}

/* vim: set ts=4 sw=4 noet: */

