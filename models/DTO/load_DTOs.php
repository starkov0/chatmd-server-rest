<?php
/*
** loadDTOs.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Load the DTOs
*/

require_once('DTO.php');

//Load all the DTOs
$files = glob(__DIR__.'/*DTO.php');
foreach($files as $file) {
	require_once($file);
}


/* vim: set ts=4 sw=4 noet: */

