<?php
/*
** DTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A DTO class
*/

abstract class DTO {

	//Converts a model to a DTO
	public static function to_DTO($obj) {

		if(!$obj) {
			return NULL;
		}

		//Are we with an array ? In that case, convert each element
		if(gettype($obj) == 'array') {
			$DTOs = [];
			foreach($obj as $DOM) {
				//converted to DTOism
				$DTOs[] = DTO::to_DTO($DOM);
			}
			return $DTOs;
		}

		//We have a single element

		//get it's class
		$class = get_class($obj);
		//Get it's DTO class
		$dto_class = $class.'DTO';

		//Create the DTO
		return new $dto_class($obj);
	}


	public static function to_json($thing) {
		return json_encode($thing);
	}

	public static function to_web($thing) {
		return DTO::to_json(DTO::to_DTO($thing));
	}
}

/* vim: set ts=4 sw=4 noet: */

