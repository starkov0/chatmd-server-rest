<?php
/*
** ConversationUserDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A ConversationUser DTO
*/

class ConversationUserDTO extends DTO {

	public $id;
	public $user_id;
	public $conversation_id;
	public $crypto_key_pwd;
	public $created_at;


	public function __construct(ConversationUser $conversation) {
		$this->id = $conversation->id;
		$this->user_id = $conversation->user_id;
		$this->conversation_id = $conversation->conversation_id;
		$this->crypto_key_pwd = $conversation->crypto_key_pwd;
		$this->created_at = $conversation->created_at;
	}

}

/* vim: set ts=4 sw=4 noet: */

