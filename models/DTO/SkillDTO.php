<?php
/*
** SkillDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A Skill DTO
*/

class SkillDTO extends DTO {

	public $id;
	public $title;
	public $description;


	public function __construct(Skill $skill) {
		$this->id = $skill->id;
		$this->title = $skill->title;
		$this->description = $skill->description;
	}

}

/* vim: set ts=4 sw=4 noet: */

