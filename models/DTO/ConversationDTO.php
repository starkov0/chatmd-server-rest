<?php
/*
** ConversationDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A Conversation DTO
*/

class ConversationDTO extends DTO {

	public $id;
	public $title;
	public $crypto_key;
	public $created_at;


	public function __construct(Conversation $conversation) {
		$this->id = $conversation->id;
		$this->title = $conversation->title;
		$this->crypto_key = $conversation->crypto_key;
		$this->created_at = $conversation->created_at;
	}

}

/* vim: set ts=4 sw=4 noet: */

