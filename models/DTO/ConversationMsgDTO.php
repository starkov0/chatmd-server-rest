<?php
/*
** ConversationMsgDTO.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A Conversation Message DTO
*/

class ConversationMsgDTO extends DTO {

	public $id;
	public $user_id;
	public $conversation_id;
	public $content;
	public $created_at;


	public function __construct(ConversationMsg $conversation) {
		$this->id = $conversation->id;
		$this->user_id = $conversation->user_id;
		$this->conversation_id = $conversation->conversation_id;
		$this->content = $conversation->content;
		$this->created_at = $conversation->created_at;
	}

}

/* vim: set ts=4 sw=4 noet: */

