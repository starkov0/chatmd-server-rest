<?php
/*
** Location.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Location of a User
*/

class Location extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  name (VARCHAR)
	//  lattitude
	//  longitude

	static $belongs_to = array(
		array('user')
	);

	static $validates_size_of = array(
		array('name', 'within' => array(1, 55))
	);
}

Authority::allow('manage', 'Location', function ($auth_user, $a_skill) {
	return $auth_user->equalsTo($a_skill->user);
});

Authority::allow('create', 'Location', function ($auth_user, $obj) {
	return $auth_user->canUseWebsite();
});

/* vim: set ts=4 sw=4 noet: */

