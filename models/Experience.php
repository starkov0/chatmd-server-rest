<?php
/*
** Experience.php - Experience(s) of a user
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: See above
*/

class Experience extends ActiveRecord\Model {
	//ATTRIBUTES
	//  id
	//  user_id (FORGEIN)
	//  title (VARCHAR)
	//  description (VARCHAR)
	//  begin (TIMESTAMP)
	//  end (TIMESTAMP)

	static $belongs_to = array(
		array('user')
	);

	static $validates_size_of = array(
		array('title', 'maximum' => 55),
		array('description', 'maximum' => 300)
	);
}


Authority::allow('manage', 'Experience', function ($auth_user, $a_experience) {
	return $auth_user->equalsTo($a_experience->user);
});

Authority::allow('create', 'Experience', function ($auth_user, $obj) {
	return $auth_user->canUseWebsite();
});

/* vim: set ts=4 sw=4 noet: */

