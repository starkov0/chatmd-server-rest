<?php
/*
** Authentication.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Authentication lib
*/


class Authentication {

	private static $_user;

	public static function initialize() {
		Authentication::$_user = new User();
		session_start();

		if(isset($_SESSION['user'])) {
			Authentication::$_user = $_SESSION['user'];
		}
	}


	public static function log_in() {
		//XXX TODO
		Authentication::$_user = UserController::get_by_id(1);
		Authentication::update_session();
	}

	public static function log_out() {
		Authentication::$_user = new User();
		Authentication::update_session();
	}

	public static function get_user() {
		//XXX TODELETE
		Authentication::$_user = UserController::get_by_id(1);


		return Authentication::$_user;
	}


	private static function update_session() {
		$_SESSION['user'] = Authentication::$_user;
	}

}

/* vim: set ts=4 sw=4 noet: */

