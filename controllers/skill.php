<?php
/*
** skill.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A general skill controller
*/

class SkillController implements iDOMController {
	static function get_by_id($skill_id) {
		return Skill::find($skill_id);
	}

	static function get_by_user($user_id) {
		return Skill::find_all_by_user_id($user_id);
	}


	static function create($skill) {
		$db = new Skill(array(
			'user' => Authentication::get_user()
		));

		SkillController::mergeAttributes($db, $skill);

		Authority::assert_can('create', $db);

		$db->save();
		return $db;
	}

	static function update($skill) {
		$db = SkillController::get_by_id($skill->id);

		SkillController::mergeAttributes($db, $skill);

		Authority::assert_can('update', $db);

		$db->save();
		return $db;
	}

	static function delete($skill_id) {
		$db = SkillController::get_by_id($skill_id);

		Authority::assert_can('delete', $db);

		$db->delete();
	}

	private static function mergeAttributes($object, $attributes) {
		$object->title = $attributes->title;
		$object->description = $attributes->description;
	}

}

/* vim: set ts=4 sw=4 noet: */

