<?php
/*
** controllers.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Load our controllers
*/

require_once('iDOMController.php');

//Load all the Controllers
$files = glob(__DIR__.'/*.php');
foreach($files as $file) {
	require_once($file);
}

/* vim: set ts=4 sw=4 noet: */

