<?php
/*
** conversation.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Controls the conversations.
*/

class ConversationController implements iDOMController {

	static function create($conversation) {
		$db = new Conversation();

		ConversationController::mergeAttributes($db, $conversation);


		Authority::assert_can('create', $db);
		$db->save();

		//Create the conversation user with the user and is admin
		$conversationUser = new ConversationUser(array(
			'conversation' => $db,
			'user' => Authentication::get_user(),
			//XXX FIXME: crypto_key_pwd ??
		));
		$conversationUser->save();

		return $db;
	}

	static function read($conversation_id) {
		$convo = Conversation::find($conversation_id);

		Authority::assert_can('read', $convo);

		return $convo;
	}

	//notused
	static function delete($conversation_id) {
		$convo = Conversation::find($conversation_id);

		Authority::assert_can('delete', $convo);

		//XXX TODO: delete messages and conversationUsers !

		$convo->delete();
	}

	private static function mergeAttributes($db, $attributes) {
		$db->title = $attributes->title;
		$db->crypto_key = $attributes->crypto_key;
	}

}

/* vim: set ts=4 sw=4 noet: */

