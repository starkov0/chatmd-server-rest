<?php
/*
** experience.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A general experience controller
*/

class ExperienceController implements iDOMController {
	static function get_by_id($experience_id) {
		return Experience::find($experience_id);
	}

	static function get_by_user($user_id) {
		return Experience::find_all_by_user_id($user_id);
	}


	static function create($experience) {

		$db = new Experience(array(
			'user' => Authentication::get_user()
		));

		ExperienceController::mergeAttributes($db, $experience);

		Authority::assert_can('create', $db);

		$db->save();
		return $db;
	}

	static function update($experience) {
		$db = ExperienceController::get_by_id($experience->id);

		ExperienceController::mergeAttributes($db, $experience);

		Authority::assert_can('update', $db);

		$db->save();
		return $db;
	}

	static function delete($experience_id) {
		$db = ExperienceController::get_by_id($experience_id);

		Authority::assert_can('delete', $db);

		$db->delete();
	}

	private static function mergeAttributes($object, $attributes) {
		$object->title = $attributes->title;
		$object->description = $attributes->description;
//		$object->begin = $attributes->begin;
//		$object->end = $attributes->end;
		$object->begin = 123;
		$object->end = 123;
		//XXX: end/begin dates
	}

}

/* vim: set ts=4 sw=4 noet: */

