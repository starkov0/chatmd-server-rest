<?php
/*
** location.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A general location controller
*/

class LocationController implements iDOMController {
	static function get_by_id($location_id) {
		return Location::find($location_id);
	}

	static function get_by_user($user_id) {
		$user = UserController::get_by_id($user_id);

		return $user->location;
	}

	static function create($Location) {

		//If we already have a location, update-it instead !
		if(Authentication::get_user()->location) {
			return static::update($Location);
		}

		$db = new Location(array(
			'user' => Authentication::get_user()
		));
		LocationController::mergeAttributes($db, $Location);

		Authority::assert_can('create', $db);

		$db->save();

		return $db;
	}

	static function update($Location) {
		$db = LocationController::get_by_id($Location->id);

		LocationController::mergeAttributes($db, $Location);

		Authority::assert_can('update', $db);

		$db->save();
		return $db;
	}

	static function delete($Location_id) {
		$db = LocationController::get_by_id($Location_id);

		Authority::assert_can('delete', $db);

		$db->delete();
	}

	private static function mergeAttributes($object, $attributes) {
		$object->name = $attributes->name;
		$object->lattitude = $attributes->lattitude;
		$object->longitude = $attributes->longitude;
	}
}

/* vim: set ts=4 sw=4 noet: */

