<?php
/*
** user.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A general user controller
*/

class UserController implements iDOMController {
	static function get_by_id($user_id) {
		return User::find($user_id);
	}

	static function search_by_name($user_name) {
		return User::find('all', array(
			'conditions' => array('name LIKE ?', '%'.$user_name.'%')));
	}

	static function create($user) {
		$db = new User(array(
			'email' => $user->email,
			'name' => $user->name,
			'title' => $user->title,
			'abstract' => $user->abstract,
			'hobbies' => $user->hobbies
		));

		$db->save();
		return $db;
	}

	static function update($user) {
		$db = UserController::get_by_id($user->id);

		/*
		if(isset($user->email)) {
			$db->email = $user->email;
		}
		 */

		$db->name = $user->name;
		$db->title = $user->title;
		$db->abstract = $user->abstract;
		$db->hobbies = $user->hobbies;

		$db->save();

		return $db;
	}

}

/* vim: set ts=4 sw=4 noet: */

