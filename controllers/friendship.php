<?php
/*
** friendship.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: A general friendship controller. NOTA: this is not a 
**              Fisherman's Friend's controller
*/

class FriendshipController implements iDOMController {
	static function get_by_id($friendship_id) {
		return Friendship::find($friendship_id);
	}


	static function create($friendship) {
		//Doest an ancient friendship REQUEST from the OTHER user exists ?
		$exists = FriendshipController::friendship_exists(
		                 $friendship->user_asked_id,
		                 $friendship->user_initiator_id);

		//Friendship already exists
		if($exists) {
			//I was already asked
			if($exists->user_asked_id == Authentication::get_user()->id) {
				//Then, validate the old request instead !
				$exists->accepted = 1;
				$exists->save();

				return $exists;
			}
		}

		//No, create a new one
		$db = new Friendship(
			array(
				'user_initiator_id' => Authentication::get_user()->id,
				'user_asked_id' => $friendship->user_asked_id,
				'comment' => $friendship->comment,
				'accepted' => 0
			)
		);

		$db->save();

		return $db;
	}

	static function update($friendship) {
		//XXX attention !
		//only the accepted flag may be edited. Must ensure
		//that the modifier is user_asked and not user_initiator !
		//We can only toggle the flag on, else it's a delete

		$db = FriendshipController::get_by_id($friendship->id);

		//XXX authorization
		if($friendship->accepted) {
			$db->accepted = 1;
		}

		$db->save();

		return $db;
	}

	//I would've used destroy friendship, but anyway… ;)
	static function delete($friendship) {
		$db = FriendshipController::get_by_id($friendship->id);

		//XXX authorization may be user_asked or user_initiator
		
		$db->delete();
	}

	//Gets all the friends of a user
	static function get_friends_of_user($user_id) {
		//XXX authorization ? Can you see the friends of everyone ?

		$friends1 = Friendship::find_all_by_user_asked_id($user_id);
		$friends2 = Friendship::find_all_by_user_initiator_id($user_id);

		return array_merge($friends1, $friends2);
	}


	private static function friendship_exists($user1_id, $user2_id) {
		$results1 = Friendship::find_all_by_user_asked_and_user_initiator(
		                         $user1_id, $user2_id);
		if($results1)
			return $results1[0]; //can only be one

		$results2 = Friendship::find_all_by_user_asked_and_user_initiator(
		                         $user2_id, $user1_id);
		if($results2)
			return $results2[0]; //can only be one

		return NULL;
	}

}

/* vim: set ts=4 sw=4 noet: */

