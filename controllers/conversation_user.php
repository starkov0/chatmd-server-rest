<?php
/*
** conversation_user.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Controls the conversations.
*/

class ConversationUserController implements iDOMController {

	static function create($conversation_user) {
		$user_to_add = User::find($conversation_user->user_id);
		$conversation = Conversation::find($conversation_user);

		$db = new ConversationUser(array(
			'user' => $user_to_add,
			'conversation' => $conversation,
			'crypto_key_pwd' => $conversation_user->crypto_key_pwd
		));

		Authority::assert_can('create', $db);
		$db->save();

		return $db;
	}


	static function delete($conversation_user_id) {
		$convoU = ConversationUser::find($conversation_user_id);

		Authority::assert_can('delete', $convoU);

		$convoU->delete();
	}


	static function all_of_user($uid) {
		if($uid != Authentication::get_user()->id) {
			//TODO: make special exception
			throw new \Exception('AUTHORIZATION: cant read their convos');
		}

		$convoUs = ConversationUser::find_all_by_user_id($uid);

		return $convoUs;
	}

	static function all_of_conversation($cid) {
		//XXX can only return if I am member of convo !

		$convoUs = ConversationUser::find_all_by_conversation_id($cid);

		return $convoUs;
	}


}

/* vim: set ts=4 sw=4 noet: */

