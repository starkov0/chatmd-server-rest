<?php
/*
** conversation_msg.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Controls the conversations messages.
*/

class ConversationMsgController implements iDOMController {

	static function create($conversation_msg) {
		$conversation = Conversation::find($conversation_msg->conversation_id);

		$db = new ConversationMsg(array(
			'user' => Authentication::get_user(),
			'conversation' => $conversation,
			'content' => $conversation_msg->content,
		));

		Authority::assert_can('create', $db);
		$db->save();

		return $db;
	}


	static function delete($message_id) {
		$msg = ConversationMsg::find($message_id);

		Authority::assert_can('delete', $msg);

		$msg->delete();
	}


	static function all_of_conversation($cid) {
		//XXX can only return if I am member of convo !

		$msgs = ConversationMsg::find_all_by_conversation_id($cid);

		return $msgs;
	}

	static function last_of_conversation($cid) {
		//XXX can only return if I am member of convo !

		$msg = ConversationMsg::find(array('conditions' => array('conversation_id = ?', $cid), 'limit' => 1, 'order' => 'id desc'));

		return $msg;
	}


}

/* vim: set ts=4 sw=4 noet: */

