<?php
/*
** index.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Index file of the application
*/

//Load our frameworks
require_once('libs/Slim/Slim.php');
require_once('libs/activerecord/ActiveRecord.php');
require_once('libs/authentication/Authentication.php');
require_once('libs/authority/Authority.php');

//Load our controllers
require_once('./controllers/load_controllers.php');
//Load our DTOs
require_once('./models/DTO/load_DTOs.php');

//Configure our frameworks
require_once('configuration.php');


$app = new \Slim\Slim(array(
	'debug' => true
));

//Authenticate the possible user
Authentication::initialize();
//Initialize the authorization
Authority::initialize(Authentication::get_user());


//load our routes
require_once('./routes/load_routes.php');

$app->response->headers->set('Content-Type', 'application/json');
//Run the app !
$app->run();

