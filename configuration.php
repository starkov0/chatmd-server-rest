<?php
/*
** configuration.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Configuration for Slim, ActiveRecord, …
*/


//We are in Paris
date_default_timezone_set('Europe/Paris');


//SLIM
\Slim\Slim::registerAutoloader();

//ActiveRecord
ActiveRecord\Config::initialize(function($cfg) {
	$cfg->set_model_directory('models');

	//FIXME for prod
	$cfg->set_connections(array(
		'development' => 'mysql://chat:chat@127.0.0.1/chat;charset=utf8'));
});

/* vim: set ts=4 sw=4 noet: */

