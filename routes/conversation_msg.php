<?php
/*
** conversation_msg.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the conversation user view: the messages
*/


## /conversation_msg is delegated to us
$app->group('/conversation_msg', function () use ($app) {

	//Ajouter un message à une conversation
	$app->post('', function() use ($app) {
		$message = $app->request->getBodyObject();

		$created = ConversationMsgController::create($message);

		$app->response()->write(DTO::to_web($created));
	});

	//Getter tous les messages d'une conversation
	$app->get('/conversation/:cid', function ($cid) use ($app) {
		$messages = ConversationMsgController::all_of_conversation($cid);

		$app->response()->write(DTO::to_web($messages));

	});

	//Getter le dernier message d'une conversation
	$app->get('/last/conversation/:cid', function ($cid) use ($app) {
		$message = ConversationMsgController::last_of_conversation($cid);

		$app->response()->write(DTO::to_web($message));

	});

	//Supprimer un message d'une conversation.
	$app->delete('/:id', function($mid) use ($app) {
		ConversationMsgController::delete($mid);

		$app->response()->write('');
	});

});

/* vim: set ts=4 sw=4 noet: */

