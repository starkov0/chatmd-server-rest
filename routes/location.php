<?php
/*
** routes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the Location view
*/


## /location is delegated to us
$app->group('/location', function () use ($app) {

	$app->post('', function() use ($app) {
		$location = $app->request->getBodyObject();

		$created = LocationController::create($location);

		$app->response()->write(DTO::to_web($created));
	});

	$app->put('/:id', function($id) use ($app) {
		$location = $app->request->getBodyObject();

		$updated = LocationController::update($location);

		$app->response()->write(DTO::to_web($updated));
	});

	$app->delete('/:id', function($id) use ($app) {
		$location = $app->request->getBodyObject();

		LocationController::update($location);

		$app->response()->write('done');
	});


	//get the location of a user
	$app->get('/user/:id', function($id) use ($app) {
		$location = LocationController::get_by_user($id);

		$app->response()->write(DTO::to_web($location));
	});
});

/* vim: set ts=4 sw=4 noet: */

