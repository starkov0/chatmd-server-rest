<?php
/*
** routes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the Skill view
*/


## /skill is delegated to us
$app->group('/skill', function () use ($app) {

	$app->post('', function() use ($app) {
		$skill = $app->request->getBodyObject();

		$created = SkillController::create($skill);

		$app->response()->write(DTO::to_web($created));
	});

	$app->put('/:id', function($id) use ($app) {
		$skill = $app->request->getBodyObject();

		$modified = SkillController::update($skill);

		$app->response()->write(DTO::to_web($modified));
	});

	$app->delete('/:id', function($id) use ($app) {
		SkillController::delete($id);
		$app->response()->write('');
	});

	##Get skills for a given user uid
	$app->get('/user/:uid', function($uid) use ($app) {
		$skills = SkillController::get_by_user($uid);

		$app->response()->write(DTO::to_web($skills));
	});



});

/* vim: set ts=4 sw=4 noet: */

