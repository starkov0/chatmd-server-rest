<?php
/*
** routes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the authentication. XXX FIXME: only a test
*/


//XXX: this is only a test. Later, we'll have to use OAuth, HTTP Basic or
//something else.

## /auth is delegated to us
$app->group('/auth', function () use ($app) {

	##Get Logged user or NULL
	$app->get('/', function() use ($app) {
		$user = Authentication::get_user();
		$app->response()->write(DTO::to_web($user));
	});

	##Login user uid
	$app->get('/login', function() use ($app) {
		//XXX TODO
		Authentication::log_in();

		$user = Authentication::get_user();
		$app->response()->write(DTO::to_web($user));
	});

	##Logout the user
	$app->get('/logout', function() use ($app) {
		Authentication::log_out();
	});

});

/* vim: set ts=4 sw=4 noet: */

