<?php
/*
** conversation_user.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the conversation user view: those who have been
**              ADDED to a conversation
*/


## /conversation_user is delegated to us
$app->group('/conversation_user', function () use ($app) {

	//Ajouter un user à une conversation
	$app->post('', function() use ($app) {
		$conversation_user = $app->request->getBodyObject();

		$created = ConversationUserController::create($conversation_user);

		$app->response()->write(DTO::to_web($created));
	});

	//Getter toutes les affiliations 'conversation-user' d'un user: toutes
	//les conversation que le user a
	$app->get('/user/:uid', function ($uid) use ($app) {
		$conversation_users = ConversationUserController::all_of_user($uid);

		$app->response()->write(DTO::to_web($conversation_users));

	});

	//Getter toutes les affiliations 'conversation-user' d'une conversation:
	//tous les users d'une conversation
	$app->get('/conversation/:cid', function ($cid) use ($app) {
		$conversation_users = ConversationUserController::all_of_conversation($cid);

		$app->response()->write(DTO::to_web($conversation_users));

	});

	//Supprimer un user d'une conversation.
	$app->delete('/:id', function($cuid) use ($app) {
		ConversationUserController::delete($cuid);

		$app->response()->write('');
	});

});

/* vim: set ts=4 sw=4 noet: */

