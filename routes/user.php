<?php
/*
** routes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the User view
*/


## /user is delegated to us
$app->group('/user', function () use ($app) {

	##Create a new user
	$app->post('', function() use ($app) {
		$input = $app->request->getBody();
		print $input;
		$user = new User(array('name' => $input, 'email' => 'toto@tutu.ch'));
		var_dump($user->save());
	});

	$app->get('/search', function() use ($app) {
		$users = UserController::search_by_name('');

		$app->response()->write(DTO::to_web($users));
	});


	$app->get('/search/:name', function($name) use ($app) {
		$users = UserController::search_by_name($name);

		$app->response()->write(DTO::to_web($users));
	});

	##Get user id
	$app->get('/:uid', function($uid) use ($app) {
		$user = UserController::get_by_id($uid);

		$app->response()->write(DTO::to_web($user));
	});

	$app->put('/:uid', function($uid) use ($app) {
		$user = $app->request->getBodyObject();

		$modified = UserController::update($user);

		$app->response->write(DTO::to_web($modified));
	});

});

/* vim: set ts=4 sw=4 noet: */

