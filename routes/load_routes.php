<?php
/*
 ** routes.php
 **
 ** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
 ** Description: Routes for our app
 */

require_once('user.php');
require_once('auth.php');
require_once('location.php');
require_once('skill.php');
require_once('experience.php');
require_once('friendship.php');
require_once('conversation.php');
require_once('conversation_user.php');
require_once('conversation_msg.php');

/* vim: set ts=4 sw=4 noet: */

