<?php
/*
** experience.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the Experience view
*/


## /experience is delegated to us
$app->group('/experience', function () use ($app) {

	$app->post('', function() use ($app) {
		$experience = $app->request->getBodyObject();

		$created = ExperienceController::create($experience);

		$app->response()->write(DTO::to_web($created));
	});

	$app->put('/:id', function($id) use ($app) {
		$experience = $app->request->getBodyObject();

		$updated = ExperienceController::update($experience);

		$app->response()->write(DTO::to_web($updated));
	});

	$app->delete('/:id', function($id) use ($app) {
		ExperienceController::delete($id);

		$app->response()->write('');
	});


	##Get experiences for a user uid
	$app->get('/user/:uid', function($uid) use ($app) {
		$experiences = ExperienceController::get_by_user($uid);

		$app->response()->write(DTO::to_web($experiences));
	});
});

/* vim: set ts=4 sw=4 noet: */

