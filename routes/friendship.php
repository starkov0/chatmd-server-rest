<?php
/*
** friendship.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the Friendship view
*/


## /friendship is delegated to us
$app->group('/friendship', function () use ($app) {

	$app->post('', function() use ($app) {
		$friendship = $app->request->getBodyObject();

		$created = FriendshipController::create($friendship);

		$app->response()->write(DTO::to_web($created));
	});

	$app->put('/:id', function($id) use ($app) {
		$friendship = $app->request->getBodyObject();

		$updated = FriendshipController::update($friendship);

		$app->response()->write(DTO::to_web($updated));
	});

	$app->delete('/:id', function($id) use ($app) {
		FriendshipController::delete($id);

		$app->response()->write('');
	});


	//get the friendships of a user
	$app->get('/user/:uid', function($uid) use ($app) {
		$friends = FriendshipController::get_friends_of_user($uid);

		$app->response()->write(DTO::to_web($friends));
	});
});

/* vim: set ts=4 sw=4 noet: */

