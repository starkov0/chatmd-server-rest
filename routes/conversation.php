<?php
/*
** conversation.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the conversation view
*/


## /conversation is delegated to us
$app->group('/conversation', function () use ($app) {

	//Créer une conversation
	$app->post('', function() use ($app) {
		$conversation = $app->request->getBodyObject();

		$created = ConversationController::create($conversation);

		$app->response()->write(DTO::to_web($created));
	});

	//Getter une conversation
	$app->get('/:id', function ($id) use ($app) {
		$conversation = ConversationController::read($id);

		$app->response()->write(DTO::to_web($conversation));

	});
});

/* vim: set ts=4 sw=4 noet: */

